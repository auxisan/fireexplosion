#include <SDL.h>
#include <iostream>
#include "Screen.h"

using namespace std;

int main(int argc, char ** argv)
{
	Screen screen;
	
	if (screen.init() == false) {
		cout << "Error initializing SDL." << endl;
	}
	

	//
	

	bool quit = false;
	SDL_Event event;

	while (!quit) {
		// Update particles
		// Draw particles
		// Check for messages/events

		while (SDL_PollEvent(&event)) {
			if (event.type == SDL_QUIT) {
				quit = true;
			}
			
		}
	}

	screen.close();

	return 0;
}